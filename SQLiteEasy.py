import sqlite3

sqlite_file = 'my_first_db.sqlite'  # name of the sqlite database file
table_name1 = 'my_table_1'  # name of the table to be created
table_name2 = 'my_table_2'  # name of the table
new_field = 'my_1st_column'  # name of the column
field_type = 'INTEGER'  # column data type

# Connecting to the database file
conn =  sqlite3.connect(sqlite_file)
c = conn.cursor()

# Creating a new SQLite table with 1 column
c.execute('CREATE TABLE {tn} ({nf} {ft})'.format(tn=table_name1, nf=new_field, ft=field_type))

# Creating second table with primary key
c.execute('CREATE TABLE {tn} ({nf} {ft} PRIMARY KEY)'.format(tn=table_name2, nf=new_field, ft=field_type))

conn.commit()
conn.close()
